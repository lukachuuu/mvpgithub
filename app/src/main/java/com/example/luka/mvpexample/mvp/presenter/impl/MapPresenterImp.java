package com.example.luka.mvpexample.mvp.presenter.impl;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.example.luka.mvpexample.mvp.presenter.MapPresenter;
import com.example.luka.mvpexample.mvp.view.IMapView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by luka on 26.8.2015..
 */
public class MapPresenterImp implements MapPresenter, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final IMapView view;
    private final Context context;
    private GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;


    public MapPresenterImp(Context context, IMapView view) {
        this.context = context;
        this.view = view;
        buildGoogleApiClient();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void loadLocation() {

        view.showMeOnMap(mLastLocation);
    }

    @Override
    public void connectMap() {
        mGoogleApiClient.connect();
    }

    @Override
    public void disconnectMap() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void reconnect() {
        mGoogleApiClient.reconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation == null) {
            //  Toast.makeText(getActivity(), "No location detected", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("MapPresenter", "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("MapPresenter", "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());

    }
}
