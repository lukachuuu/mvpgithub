package com.example.luka.mvpexample.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.luka.mvpexample.ui.fragment.TutorialScreenFragment;
import com.example.luka.mvpexample.ui.fragment.WelcomeTourFragment;

/**
 * Created by luka on 18.8.2015..
 */
public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    static final int NUM_PAGES = 3;

    public ScreenSlidePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        WelcomeTourFragment tp = null;
        switch (position) {
            case 0:
//                tp = WelcomeTourFragment.newInstance(R.layout.welcome_fragment01);
//                break;
                return TutorialScreenFragment.newInstance(position);
            case 1:
//                tp = WelcomeTourFragment.newInstance(R.layout.welcome_fragment02);
//                break;
                return TutorialScreenFragment.newInstance(position);
            case 2:
                return WelcomeTourFragment.newInstance();
//                break;

        }

        return tp;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
