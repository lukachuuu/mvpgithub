package com.example.luka.mvpexample.mvp.view;

import com.example.luka.mvpexample.domain.model.User;

import java.util.List;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUsersListView {

    void showUsers(List<User> users);

    void showUserDetail(User userExtra);
}
