package com.example.luka.mvpexample.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.mvp.view.IProgressView;
import com.example.luka.mvpexample.ui.activity.MainActivity;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import butterknife.ButterKnife;

/**
 * Created by luka on 18.8.2015..
 */
public class WelcomeTourFragment extends Fragment implements IProgressView, View.OnClickListener {

    private static final String TAG = WelcomeTourFragment.class.getSimpleName();

    private CallbackManager callbackManager;
    private ProfileTracker mProfileTracker;

    private View mRootView;

    private OnFragmentInteractionListener mListener;

    protected MaterialDialog dialog;


    protected LoginButton facebook_login;
    private Button btnLogin;
    private Button btnSignUp;

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("Success FB login", "onSuccess");

            Profile profile = Profile.getCurrentProfile();
            Log.d("FB Profile", "onSuccess, profile = " + profile);

            String img = profile.getProfilePictureUri(200, 200).toString();


            PrefUtil.saveUserEmail("email", getActivity());
            PrefUtil.saveUserImg(img, getActivity());


            Util.startActivityWithClearingActivityStack(getActivity(), MainActivity.class);
        }

        @Override
        public void onCancel() {
            showError("Login Cancel");
        }

        @Override
        public void onError(FacebookException e) {
            showError("onError, exception = " + e.toString());
        }
    };

    final static String LAYOUT_ID = "layoutid";

    public static WelcomeTourFragment newInstance() {
        return new WelcomeTourFragment();
    }

    public static WelcomeTourFragment newInstance(int layoutId) {

        WelcomeTourFragment pane = new WelcomeTourFragment();
        Bundle args = new Bundle();
        args.putInt(LAYOUT_ID, layoutId);
        pane.setArguments(args);
        return pane;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //facebook init?

        saveInfoToPreferences();

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, mFacebookCallback);

        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

                if (newProfile != null) {
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    Log.d("Response FB", "onCompleted, response = " + response);
                                }
                            });

                    request.executeAsync();
                }
            }
        };
    }

    private void saveInfoToPreferences() {
        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        preferences.edit().putBoolean("USER_SAW_LOGIN_SCREEN", true).apply();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.welcome_fragment03, container, false);
        mRootView = rootView;
        ButterKnife.bind(this, rootView);

        try {

            btnLogin = (Button) rootView.findViewById(R.id.btn_login);
            btnSignUp = (Button) rootView.findViewById(R.id.btn_sign_up);

            btnLogin.setOnClickListener(this);
            btnSignUp.setOnClickListener(this);

            LoginButton facebook_login = (LoginButton) rootView.findViewById(R.id.btn_facebook_login);


            facebook_login.setCompoundDrawables(null, null, null, null);
            facebook_login.setBackgroundResource(android.R.color.transparent);
            facebook_login.setFragment(WelcomeTourFragment.this);
            facebook_login.setReadPermissions("public_profile", "email");
            facebook_login.registerCallback(callbackManager, mFacebookCallback);
        } catch (Exception e) {
            String err = (e.getMessage() == null) ? "Fail load" : e.getMessage();
            Log.e("load fragment fail: ", err);
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showProgress() {
        dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .show();

    }

    @Override
    public void showProgress(String title, String text) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(text)
                .progress(true, 0)
                .show();
    }

    @Override
    public void hideProgress() {
        this.dialog.hide();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("Ok")
                .show();
    }

    @Override
    public void onClick(View v) {
        mListener.onButtonClicked(v.getId());
    }

    public interface OnFragmentInteractionListener {
        void onButtonClicked(int btnId);
    }
}
