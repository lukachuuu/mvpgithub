package com.example.luka.mvpexample.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.mvp.presenter.LoginPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.LoginPresenterImp;
import com.example.luka.mvpexample.mvp.view.LoginView;
import com.example.luka.mvpexample.ui.activity.MainActivity;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;

import butterknife.Bind;
import butterknife.ButterKnife;


public class LoginFragment extends Fragment implements LoginView {

    @Bind(R.id.btnLogin)
    public Button btnLogin;

    @Bind(R.id.et_email)
    public EditText email;

    @Bind(R.id.et_password)
    public EditText password;

    private LoginPresenter presenter;

    MaterialDialog dialog;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login, container, false);

        ButterKnife.bind(this, v);

        if (PrefUtil.hasUserEmail(getActivity())) {
            Util.startActivityWithClearingActivityStack(getActivity(), MainActivity.class);
        }

        presenter = new LoginPresenterImp(getActivity(), this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateUserData(email.getText().toString(), password.getText().toString());
            }
        });

        return v;
    }


    @Override
    public void navigateToHome() {
        Util.startActivityWithClearingActivityStack(getActivity(), MainActivity.class);
    }

    @Override
    public void showProgress() {
        dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .show();

    }

    @Override
    public void showProgress(String title, String text) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(text)
                .progress(true, 0)
                .show();
    }

    @Override
    public void hideProgress() {
        this.dialog.hide();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("Ok")
                .show();
    }
}