package com.example.luka.mvpexample.mvp.presenter.impl;

import android.content.Context;

import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.presenter.GitUsersListPresenter;
import com.example.luka.mvpexample.mvp.view.GitUsersListView;
import com.example.luka.mvpexample.mvp.view.IProgressView;
import com.example.luka.mvpexample.network.RestClient;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by luka on 7.8.2015..
 */

public class GitUsersListPresenterImp implements GitUsersListPresenter {

    private final GitUsersListView view;
    private final Context context;
    private final IProgressView progress;

    public GitUsersListPresenterImp(Context context, GitUsersListView view, IProgressView progress) {
        this.context = context;
        this.view = view;
        this.progress = progress;
    }

    @Override
    public void loadGitUserList() {

        progress.showProgress("Please wait", "Loading user data");
        RestClient.getInstance().getService().getUsers(new Callback<List<User>>() {
            @Override
            public void success(List<User> userList, Response response) {
                progress.hideProgress();
                view.showUsers(userList);
            }

            @Override
            public void failure(RetrofitError error) {
                progress.hideProgress();
                progress.showError(error.getMessage());
            }
        });
    }

    @Override
    public void onUserSelected(User user) {
        view.showUserDetail(user);
    }

}
