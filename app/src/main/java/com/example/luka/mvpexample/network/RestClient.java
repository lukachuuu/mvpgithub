package com.example.luka.mvpexample.network;

import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by luka on 10.8.2015..
 */
public class RestClient {

    private static volatile RestClient instance = null;
    private static final String GITHUB_API = "https://api.github.com";

    private GitHubService service;

    private RestClient() {
        configRetrofit();
    }

    public static RestClient getInstance() {
        if (instance == null) {
            synchronized (RestClient.class) {
                if (instance == null) {
                    instance = new RestClient();
                }
            }
        }

        return instance;
    }

    private void configRetrofit() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(GITHUB_API)
                .setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String message) {
                        Log.d("REST-ADAPTER", message);
                    }
                })
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "application/json");
                    }
                })
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        service = restAdapter.create(GitHubService.class);
    }

    public GitHubService getService() {
        return service;
    }
}
