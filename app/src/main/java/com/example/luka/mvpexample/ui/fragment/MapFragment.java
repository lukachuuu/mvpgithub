package com.example.luka.mvpexample.ui.fragment;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.mvp.presenter.MapPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.MapPresenterImp;
import com.example.luka.mvpexample.mvp.view.IMapView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.listeners.ActionClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MapFragment extends Fragment implements IMapView {

    private static final String TAG = MapFragment.class.getSimpleName();

    GoogleMap googleMap;
    MapView mapView;

    protected MapPresenter presenter;

    @Bind(R.id.fabMe)
    FloatingActionButton fabMe;

    public MapFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new MapPresenterImp(getActivity(), this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_map, container, false);
        mapView = (MapView) v.findViewById(R.id.mapView);

        ButterKnife.bind(this, v);

        mapView.onCreate(savedInstanceState);

        mapView.onResume();// needed to get the map to display immediately

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Browse spaces");

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mapView.getMap();
        // Zagreb
        double latitude = 45.8167;
        double longitude = 15.9833;

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(9).build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        fabMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadLocation();
            }
        });

        return v;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onStart() {
        super.onStart();
        presenter.connectMap();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.disconnectMap();
    }


    @Override
    public void showMeOnMap(Location mlocation) {
        if (mlocation != null) {
            MarkerOptions marker = new MarkerOptions().position(new LatLng(mlocation.getLatitude(), mlocation.getLongitude())).title("You are here!");

            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

            googleMap.addMarker(marker);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mlocation.getLatitude(), mlocation.getLongitude())).zoom(12).build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            Snackbar.with(getActivity())
                    .text("Please check location settings")
                    .actionLabel("Here")
                    .actionListener(new ActionClickListener() {
                        @Override
                        public void onActionClicked(Snackbar snackbar) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                        }
                    })
                    .show(getActivity());
            presenter.reconnect();
        }
    }
}
