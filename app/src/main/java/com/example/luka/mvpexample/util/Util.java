package com.example.luka.mvpexample.util;


import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.ui.activity.MainActivity;

/**
 * Created by luka on 11.8.2015..
 */
public class Util {

    private static final String TAG = Util.class.getSimpleName();

    public static void startActivityWithClearingActivityStack(Activity callingActivity, Class activityToStart) {
        startActivityWithClearingActivityStack(callingActivity, activityToStart, false);
    }

    public static void startActivityWithClearingActivityStack(Activity callingActivity, Class<MainActivity> activityToStart, boolean noActivityTransition) {

        Intent intent = new Intent(callingActivity, activityToStart);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        callingActivity.startActivity(intent);
        if (noActivityTransition) {
            callingActivity.overridePendingTransition(0, 0);
        }
    }

    public static void performReplaceFragment(FragmentManager fm, Fragment fragment, String tag) {

        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);
        ft.replace(R.id.fragment_placeholder_tutorial, fragment, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public static void addMapFragment(FragmentManager fm) {
        FragmentTransaction transaction = fm.beginTransaction();
        Fragment fragment = new Fragment();
        transaction.replace(R.id.fragment_placeholder, fragment);
        transaction.commit();
    }


}
