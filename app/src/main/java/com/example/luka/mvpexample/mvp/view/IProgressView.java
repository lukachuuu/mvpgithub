package com.example.luka.mvpexample.mvp.view;

/**
 * Created by luka on 12.8.2015..
 */
public interface IProgressView {

    void showProgress();

    void showProgress(String title, String text);

    void hideProgress();

    void showError(String error);
}
