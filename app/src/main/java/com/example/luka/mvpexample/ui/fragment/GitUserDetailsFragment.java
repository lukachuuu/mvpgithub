package com.example.luka.mvpexample.ui.fragment;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.presenter.GitUserDetailsPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.GitUserDetailsPresenterImp;
import com.example.luka.mvpexample.mvp.view.GitUserDetailsView;
import com.example.luka.mvpexample.mvp.view.IProgressView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;


public class GitUserDetailsFragment extends BaseFragment implements GitUserDetailsView, IProgressView {

    @Bind(R.id.tv_username)
    protected TextView username;

    @Bind(R.id.tv_id)
    protected TextView id;

    @Bind(R.id.tv_url)
    protected TextView url;

    @Bind(R.id.tv_type)
    protected TextView type;

    @Bind(R.id.tv_admin)
    protected TextView admin;

    @Bind(R.id.iv_avatar)
    protected ImageView avatar;

    private GitUserDetailsPresenter presenter;

    protected MaterialDialog dialog;


    public GitUserDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_git_user_details, container, false);

        ButterKnife.bind(this, v);

        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        presenter = new GitUserDetailsPresenterImp(getActivity(), this, this);

        Bundle bundle = this.getArguments();
        User user = bundle.getParcelable("key");

        presenter.loadDetails(user);

        return v;
    }


    @Override
    public void showUser(User user) {
        username.setText("Username: " + user.getLogin());
        id.setText("ID: " + user.getId().toString());
        url.setText(user.getUrl());
        type.setText("Type user: " + user.getType());
        admin.setText("Site Admin: " + user.getSiteAdmin().toString());

        Picasso.with(getActivity()).load(user.getAvatarUrl()).into(avatar);
    }

    @Override
    public void showProgress() {
        dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .show();

    }

    @Override
    public void showProgress(String title, String text) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(text)
                .progress(true, 0)
                .show();
    }

    @Override
    public void hideProgress() {
        this.dialog.hide();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("Ok")
                .show();
    }


}
