package com.example.luka.mvpexample.mvp.presenter.impl;

import android.content.Context;

import com.example.luka.mvpexample.database.DbUtil;
import com.example.luka.mvpexample.mvp.presenter.SignUpPresenter;
import com.example.luka.mvpexample.mvp.view.SignUpView;
import com.example.luka.mvpexample.util.ValidationUtil;

/**
 * Created by luka on 20.8.2015..
 */
public class SignUpPresenterImp implements SignUpPresenter {

    private SignUpView view;
    private Context context;

    public SignUpPresenterImp(Context context, SignUpView view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void validateUserData(String email, String password, String name, String surname) {

        view.showProgress();


        if (!ValidationUtil.isEmailValid(email)) {
            view.hideProgress();
            view.showError("Your email is invalid!");
            return;
        } else if (!ValidationUtil.isPasswordValid(password)) {
            view.hideProgress();
            view.showError("Your password is invalid! It must be at least 6 character long!");
            return;
        } else if (ValidationUtil.isTextEmpty(name)) {
            view.hideProgress();
            view.showError("Field with name can't be empty");
        } else if (ValidationUtil.isTextEmpty(surname)) {
            view.hideProgress();
            view.showError("Field with surname can't be empty");
        } else {
            view.hideProgress();
            DbUtil.signUpUser(context, email, password, name, surname);
            view.navigateToLogin();
        }
    }
}
