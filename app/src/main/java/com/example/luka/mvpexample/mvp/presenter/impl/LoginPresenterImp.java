package com.example.luka.mvpexample.mvp.presenter.impl;

import android.content.Context;

import com.example.luka.mvpexample.database.AppUser;
import com.example.luka.mvpexample.database.DbUtil;
import com.example.luka.mvpexample.mvp.presenter.LoginPresenter;
import com.example.luka.mvpexample.mvp.view.LoginView;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.ValidationUtil;

/**
 * Created by luka on 10.8.2015..
 */
public class LoginPresenterImp implements LoginPresenter {

    private final LoginView view;
    private Context context;


    public LoginPresenterImp(Context context, LoginView view) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void validateUserData(String email, String password) {
        view.showProgress();

        if (!ValidationUtil.isEmailValid(email)) {
            view.hideProgress();
            view.showError("Your email is invalid!");
            return;
        } else if (!ValidationUtil.isPasswordValid(password)) {
            view.hideProgress();
            view.showError("Your password is invalid! It must be at least 6 character long!");
            return;
        } else {
            view.hideProgress();
            AppUser user = DbUtil.userExist(context, email, password);
            if (user != null) {
                PrefUtil.saveUserEmail(email, context);
                view.navigateToHome();
            } else {
                view.showError("User doesn't exist!");
            }
        }


    }

}
