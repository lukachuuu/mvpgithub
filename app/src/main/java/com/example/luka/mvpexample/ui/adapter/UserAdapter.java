package com.example.luka.mvpexample.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by luka on 7.8.2015..
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<User> userList;

    public UserAdapter(List<User> userList) {
        this.userList = userList;
    }

    private UserClickListener userClickListener;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user, parent, false);
        return new ViewHolder(v);
    }

    /**
     * Put stuff for show
     *
     * @param holder
     * @param position
     */

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final User user = userList.get(position);

        holder.userId.setText(user.getId().toString());
        holder.username.setText(user.getLogin().toString());
        holder.url.setText(user.getUrl().toString());

        Context context = holder.avatar.getContext();

        Picasso.with(context).load(user.getAvatarUrl()).transform(new CircleTransform()).into(holder.avatar);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("CardClick", user.getLogin());
                if (userClickListener != null) {
                    userClickListener.onUserClicked(user);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.userList.size();
    }


    public void setUserClickListener(UserClickListener userClickListener) {
        this.userClickListener = userClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View rootView;

        @Bind(R.id.user_id)
        TextView userId;

        @Bind(R.id.username)
        TextView username;

        @Bind(R.id.iv_poster_image)
        ImageView avatar;

        @Bind(R.id.url)
        TextView url;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            ButterKnife.bind(this, itemView);
        }

    }

    public interface UserClickListener {

        public void onUserClicked(User user);
    }

    public void add(User item, int position) {
        userList.add(position, item);
        notifyItemInserted(position);
    }

    public void sort() {


        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User lhs, User rhs) {
                return lhs.getLogin().toLowerCase().compareTo(rhs.getLogin().toLowerCase());
            }
        });

        notifyDataSetChanged();
    }


}


