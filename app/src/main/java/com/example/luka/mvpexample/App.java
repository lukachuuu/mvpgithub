package com.example.luka.mvpexample;

import android.app.Application;

/**
 * Created by luka on 6.8.2015..
 */
public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }
}