package com.example.luka.mvpexample.mvp.presenter;

/**
 * Created by luka on 20.8.2015..
 */
public interface SignUpPresenter {

    void validateUserData(String email, String password, String name, String surname);
}
