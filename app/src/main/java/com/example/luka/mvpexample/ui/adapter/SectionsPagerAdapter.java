package com.example.luka.mvpexample.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.luka.mvpexample.ui.fragment.TutorialScreenFragment;
import com.example.luka.mvpexample.ui.fragment.WelcomeTourFragment;

/**
 * Created by luka on 25.8.2015..
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position < 2)
            return TutorialScreenFragment.newInstance(position);
        else
            return WelcomeTourFragment.newInstance();
    }

    @Override
    public int getCount() {
        return 3;
    }
}
