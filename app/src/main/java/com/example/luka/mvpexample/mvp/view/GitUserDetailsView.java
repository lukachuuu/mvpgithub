package com.example.luka.mvpexample.mvp.view;

import com.example.luka.mvpexample.domain.model.User;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUserDetailsView {

    void showUser(User user);

}
