package com.example.luka.mvpexample.mvp.presenter;

/**
 * Created by luka on 26.8.2015..
 */
public interface MapPresenter {

    void loadLocation();

    void connectMap();

    void disconnectMap();

    void reconnect();
}
