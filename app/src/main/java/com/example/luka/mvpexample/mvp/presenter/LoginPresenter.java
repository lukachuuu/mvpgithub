package com.example.luka.mvpexample.mvp.presenter;

/**
 * Created by luka on 10.8.2015..
 */
public interface LoginPresenter {

    void validateUserData(String email, String password);

}
