package com.example.luka.mvpexample.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.mvp.presenter.LoginPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.LoginPresenterImp;
import com.example.luka.mvpexample.mvp.view.LoginView;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;
import com.facebook.appevents.AppEventsLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by luka on 10.8.2015..
 */
public class LoginActivity extends BaseActivity implements LoginView {

    @Bind(R.id.btnLogin)
    public Button btnLogin;

    @Bind(R.id.et_email)
    public EditText email;

    @Bind(R.id.et_password)
    public EditText password;

    private LoginPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (PrefUtil.hasUserEmail(getApplicationContext())) {
            Util.startActivityWithClearingActivityStack(LoginActivity.this, MainActivity.class);
        }

        presenter = new LoginPresenterImp(getApplicationContext(), this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateUserData(email.getText().toString(), password.getText().toString());
            }
        });

    }


    @Override
    public void navigateToHome() {

        Util.startActivityWithClearingActivityStack(LoginActivity.this, MainActivity.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @OnClick(R.id.testBttn)
    public void startTour() {
        Intent intent = new Intent(this, TourActivity.class);
        startActivity(intent);
        finish();
    }


}
