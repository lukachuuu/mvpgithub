package com.example.luka.mvpexample.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luka.mvpexample.R;

/**
 * Created by luka on 25.8.2015..
 */
public class TutorialScreenFragment extends Fragment {

    private static final String TAG = TutorialScreenFragment.class.getSimpleName();

    private static final int[] BACKGROUND_COLORS = {R.color.background1, R.color.background2};

    private static final int[] TEST_IMG = {R.drawable.ic_android_white_48dp, R.drawable.ic_favorite_border_white_48dp};

    private static final String[] TITLES = {"Welcome message", "Welcome message"};

    private static final String[] SUBTITLES = {"No1", "No2"};

    private static final String ARG_SECTION_NUMBER = "section_number";
    private int sectionNumber;

    public TutorialScreenFragment() {

    }

    public static TutorialScreenFragment newInstance(int sectionNumber) {
        TutorialScreenFragment fragment = new TutorialScreenFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial_screen, container, false);

        TextView title = (TextView) rootView.findViewById(R.id.tv_title);
        title.setText(TITLES[sectionNumber % TITLES.length]);

        TextView subtitle = (TextView) rootView.findViewById(R.id.tv_subtitle);
        subtitle.setText(SUBTITLES[sectionNumber % SUBTITLES.length]);

        ImageView testImg = (ImageView) rootView.findViewById(R.id.testImg);
        testImg.setImageResource(TEST_IMG[sectionNumber % TEST_IMG.length]);

        ImageView imageView = (ImageView) rootView.findViewById(R.id.iv_background);
        if (sectionNumber == 0)
            imageView.setBackgroundColor(getResources().getColor(R.color.background1));
        else
            imageView.setBackgroundColor(getResources().getColor(R.color.background2));


        return rootView;
    }


}
