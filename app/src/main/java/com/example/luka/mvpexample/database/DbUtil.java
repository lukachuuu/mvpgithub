package com.example.luka.mvpexample.database;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by luka on 20.8.2015..
 */
public class DbUtil {

    public static AppUser userExist(Context context, String email, String password) {
        Realm realm = Realm.getInstance(context);
        RealmQuery<AppUser> query = realm.where(AppUser.class);
        query.equalTo("email", email);
        query.equalTo("password", password);

        AppUser user = new AppUser();

        AppUser result = query.findFirst();

        //why this? :S
        if (result != null) {
            user.setEmail(result.getEmail());
            user.setName(result.getName());
            user.setSurname(result.getSurname());
            user.setPassword(result.getPassword());

            return user;

        }
        return null;

    }

    public static void signUpUser(Context context, String email, String password, String name, String surname) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();

        AppUser appUser = realm.createObject(AppUser.class);

        appUser.setPassword(password);
        appUser.setEmail(email);
        appUser.setSurname(surname);
        appUser.setName(name);

        realm.commitTransaction();

        return;
    }
}
