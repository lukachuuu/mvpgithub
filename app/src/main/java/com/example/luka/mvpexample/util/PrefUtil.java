package com.example.luka.mvpexample.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Created by luka on 17.8.2015..
 */
public class PrefUtil {

    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_IMG = "USER_IMG";

    public static boolean hasUserEmail(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return !sp.getString(USER_EMAIL, "").equalsIgnoreCase("");

    }

    public static void saveUserEmail(final String email, final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(USER_EMAIL, email).commit();
    }

    public static String getUserEmail(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(USER_EMAIL, "");
    }


    public static void deleteUserEmail(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(USER_EMAIL);
        editor.commit();
    }

    public static void saveUserImg(final String url, final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(USER_IMG, url).commit();
    }

    public static String getUserImg(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(USER_IMG, "");
    }

    public static void deleteUserImg(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(USER_IMG);
        editor.commit();
    }
}
