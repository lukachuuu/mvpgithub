package com.example.luka.mvpexample.mvp.view;

/**
 * Created by luka on 10.8.2015..
 */
public interface LoginView extends BaseView {

    public void navigateToHome();
}
