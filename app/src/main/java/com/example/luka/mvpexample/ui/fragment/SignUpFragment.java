package com.example.luka.mvpexample.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.mvp.presenter.SignUpPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.SignUpPresenterImp;
import com.example.luka.mvpexample.mvp.view.SignUpView;
import com.example.luka.mvpexample.ui.activity.MainActivity;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.listeners.ActionClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SignUpFragment extends Fragment implements SignUpView {

    @Bind(R.id.btnReg)
    Button btnReg;

    @Bind(R.id.et_email_signup)
    EditText email;

    @Bind(R.id.et_password_signup)
    EditText password;

    @Bind(R.id.et_name)
    EditText name;

    @Bind(R.id.et_surname)
    EditText surname;

    private SignUpPresenter presenter;

    MaterialDialog dialog;

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    public SignUpFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_sign_up, container, false);


        ButterKnife.bind(this, v);


        if (PrefUtil.hasUserEmail(getActivity())) {
            Util.startActivityWithClearingActivityStack(getActivity(), MainActivity.class);
        }

        presenter = new SignUpPresenterImp(getActivity(), this);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateUserData(email.getText().toString(), password.getText().toString(), name.getText().toString(), surname.getText().toString());
            }
        });

        return v;

    }


    @Override
    public void navigateToLogin() {


        Snackbar.with(getActivity())
                .text("Registration successful!")
                .actionLabel("Redirect")
                .actionListener(new ActionClickListener() {
                    @Override
                    public void onActionClicked(Snackbar snackbar) {
                        LoginFragment fragment = new LoginFragment();
                        Util.performReplaceFragment(getFragmentManager(), fragment, "login_fragment");
                    }
                })
                .show(getActivity());

    }

    @Override
    public void showProgress() {
        dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .show();

    }

    @Override
    public void showProgress(String title, String text) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(text)
                .progress(true, 0)
                .show();
    }

    @Override
    public void hideProgress() {
        this.dialog.hide();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("Ok")
                .show();
    }
}
