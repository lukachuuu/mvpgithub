package com.example.luka.mvpexample.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by luka on 6.8.2015..
 */
public class User implements Serializable, Parcelable {

    @SerializedName("login")
    private String login;

    @SerializedName("id")
    private Integer id;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @Expose
    @SerializedName("html_url")
    private String url;

    @Expose
    @SerializedName("type")
    private String type;

    @SerializedName("site_admin")
    @Expose
    private Boolean siteAdmin;

    protected User(Parcel in) {
        login = in.readString();
        avatarUrl = in.readString();
        url = in.readString();
        type = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(String login, Integer id, String avatarUrl, String url, String type, Boolean siteAdmin) {
        this.login = login;
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.url = url;
        this.type = type;
        this.siteAdmin = siteAdmin;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getSiteAdmin() {
        return siteAdmin;
    }

    public void setSiteAdmin(Boolean siteAdmin) {
        this.siteAdmin = siteAdmin;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.login);
        dest.writeString(this.type);
        dest.writeString(this.url);
        dest.writeString(this.siteAdmin.toString());
    }
}
