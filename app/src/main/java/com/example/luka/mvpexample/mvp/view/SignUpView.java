package com.example.luka.mvpexample.mvp.view;

/**
 * Created by luka on 20.8.2015..
 */
public interface SignUpView extends BaseView {

    void navigateToLogin();
}
