package com.example.luka.mvpexample.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.presenter.GitUsersListPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.GitUsersListPresenterImp;
import com.example.luka.mvpexample.mvp.view.GitUsersListView;
import com.example.luka.mvpexample.mvp.view.IProgressView;
import com.example.luka.mvpexample.ui.adapter.UserAdapter;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class GitUsersListFragment extends BaseFragment implements GitUsersListView, IProgressView, UserAdapter.UserClickListener {

    @Bind(R.id.recycler_user_list)
    protected RecyclerView userRecycleList;

    protected MaterialDialog dialog;

    protected GitUsersListPresenter gitUsersListPresenter;

    protected UserAdapter userAdapter;

    private View v;

    @Bind(R.id.fab)
    protected FloatingActionButton fab;


    public static GitUsersListFragment newInstance() {
        GitUsersListFragment fragment = new GitUsersListFragment();
        return fragment;
    }

    public GitUsersListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_git_users_list, container, false);

        ButterKnife.bind(this, v);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Find a space");

        userRecycleList.setHasFixedSize(true);
        userRecycleList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

        gitUsersListPresenter = new GitUsersListPresenterImp(getActivity().getApplicationContext(), this, this);

        if (userAdapter != null)
            this.userRecycleList.setAdapter(userAdapter);
        else {

            gitUsersListPresenter.loadGitUserList();

        }

        fab.attachToRecyclerView(userRecycleList);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                User usertmp = new User("test",2333,"http://s3.amazonaws.com/37assets/svn/765-default-avatar.png","www.google.com","admin",true);
//                userAdapter.add(usertmp, userAdapter.getItemCount() );
                if (userAdapter != null) userAdapter.sort();
            }
        });


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    @Override
    public void showUsers(List<User> users) {
        userAdapter = new UserAdapter(users);
        userAdapter.setUserClickListener(this);
        userAdapter.notifyDataSetChanged();
        userRecycleList.setAdapter(userAdapter);
    }

    @Override
    public void showUserDetail(User userExtra) {

        GitUserDetailsFragment fragment = new GitUserDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("key", userExtra);
        replaceFragment(fragment, bundle);

    }

    @Override
    public void onUserClicked(User user) {
        gitUsersListPresenter.onUserSelected(user);
    }


    @Override
    public void showProgress() {
        dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .widgetColor(getResources().getColor(R.color.primary))
                .show();

    }

    @Override
    public void showProgress(String title, String text) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(text)
                .progress(true, 0)
                .widgetColor(getResources().getColor(R.color.primary))
                .show();
    }

    @Override
    public void hideProgress() {
        this.dialog.hide();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("Ok")
                .show();
    }

    private void replaceFragment(Fragment fragment, Bundle bundle) {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_placeholder, fragment);

        fragment.setArguments(bundle);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
