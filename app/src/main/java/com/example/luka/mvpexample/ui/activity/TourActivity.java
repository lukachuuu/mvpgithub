package com.example.luka.mvpexample.ui.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.ui.adapter.ScreenSlidePagerAdapter;
import com.example.luka.mvpexample.ui.fragment.LoginFragment;
import com.example.luka.mvpexample.ui.fragment.SignUpFragment;
import com.example.luka.mvpexample.ui.fragment.WelcomeTourFragment;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;
import com.facebook.FacebookSdk;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by luka on 18.8.2015..
 */
public class TourActivity extends AppCompatActivity implements WelcomeTourFragment.OnFragmentInteractionListener {

    static final int NUM_PAGES = 3;

    private static final String TAG = TourActivity.class.getSimpleName();

    public CircleIndicator defaultIndicator;

    ViewPager pager;
    PagerAdapter pagerAdapter;
    PagerAdapter pagerAdapter2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        FacebookSdk.sdkInitialize(this);

        if (PrefUtil.hasUserEmail(getApplicationContext())) {
            Util.startActivityWithClearingActivityStack(this, MainActivity.class);
        }

        setContentView(R.layout.activity_tutorial);

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        defaultIndicator = (CircleIndicator) findViewById(R.id.indicator_default);

        pager.setPageTransformer(false, new FadeInOutPageTransformer());


        pager.setOffscreenPageLimit(3);
        pager.setAdapter(pagerAdapter);

        defaultIndicator.setViewPager(pager);


        if (userWasOnLoginScreen()) {
            pager.setCurrentItem(3);
        }


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private boolean userWasOnLoginScreen() {
        SharedPreferences preferences = this.getPreferences(Context.MODE_PRIVATE);
        return preferences.getBoolean("USER_SAW_LOGIN_SCREEN", false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pager != null) {
            pager.clearOnPageChangeListeners();
        }
    }


    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            LoginFragment loginfragment = (LoginFragment) getSupportFragmentManager().findFragmentByTag("login_fragment");
            SignUpFragment signUpFragment = (SignUpFragment) getSupportFragmentManager().findFragmentByTag("signup_fragment");
            if ((loginfragment != null && loginfragment.isVisible()) || (signUpFragment != null && signUpFragment.isVisible())) {
                super.onBackPressed();

            } else {
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        }
    }

    @Override
    public void onButtonClicked(int btnId) {
        switch (btnId) {
            case R.id.btn_facebook_login:
                Log.d(TAG, "onButtonClicked, facebook");
                break;
            case R.id.btn_login:
                Log.d(TAG, "onButtonClicked, LOGIN");
                Util.performReplaceFragment(getSupportFragmentManager(), LoginFragment.newInstance(), "login_fragment");
                break;
            case R.id.btn_sign_up:
                Log.d(TAG, "onButtonClicked, Registration");
                Util.performReplaceFragment(getSupportFragmentManager(), SignUpFragment.newInstance(), "signup_fragment");
                break;

        }
    }


    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {

        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }


    }

    public class FadeInOutPageTransformer implements ViewPager.PageTransformer {
        @Override
        public void transformPage(View page, float position) {
            int pageWidth = page.getWidth();

            RelativeLayout root = (RelativeLayout) page.findViewById(R.id.root_view);
//            ImageView testImg = (ImageView) root.findViewById(R.id.testImg);

            if (position <= 1) {
                page.setTranslationX(pageWidth * -position);
                setTranslationOnChildren(root, pageWidth * position);

                if (position <= 0) {
                    root.setAlpha(1f + position);
                } else {
                    root.setAlpha(1f - position);
                }
            }
        }

        private void setTranslationOnChildren(ViewGroup root, float offset) {
            int childCount = root.getChildCount();
            // first child is background ivIcon (at index 0), so we skipp it
            for (int i = 1; i < childCount; i++) {
                root.getChildAt(i).setTranslationX(offset);
            }
        }
    }
}