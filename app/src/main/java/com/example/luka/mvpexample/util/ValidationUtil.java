package com.example.luka.mvpexample.util;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by luka on 20.8.2015..
 */
public class ValidationUtil {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final Integer PASSWORD_MIN_LENGTH = 6;

    public static boolean isEmailValid(String email) {
        if (email == null || email.trim().isEmpty()) return false;
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isEmailValid(EditText etEmail) {
        return isEmailValid(etEmail.getText().toString());
    }

    public static boolean isPasswordValid(String password) {
        if (password == null || password.isEmpty()) return false;
        if (password.length() < PASSWORD_MIN_LENGTH) return false;

        return true;
    }

    public static boolean isPasswordValid(EditText etPassword) {
        return isPasswordValid(etPassword.getText().toString());
    }

    public static boolean isEditTextEmpty(EditText editText) {
        return isTextEmpty(editText.getText().toString());
    }

    public static boolean isTextEmpty(String s) {
        return s.trim().isEmpty();
    }
}
