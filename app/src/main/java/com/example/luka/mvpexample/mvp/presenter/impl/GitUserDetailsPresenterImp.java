package com.example.luka.mvpexample.mvp.presenter.impl;

import android.content.Context;

import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.presenter.GitUserDetailsPresenter;
import com.example.luka.mvpexample.mvp.view.GitUserDetailsView;
import com.example.luka.mvpexample.mvp.view.IProgressView;
import com.example.luka.mvpexample.network.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by luka on 7.8.2015..
 */


public class GitUserDetailsPresenterImp implements GitUserDetailsPresenter {

    private final GitUserDetailsView view;

    private IProgressView progressView;

    private Context context;

    public GitUserDetailsPresenterImp(Context context, GitUserDetailsView view, IProgressView progressView) {
        this.view = view;
        this.progressView = progressView;
        this.context = context;
    }

    @Override
    public void loadDetails(User user) {

        progressView.showProgress();
        RestClient.getInstance().getService().getUser(user.getLogin().toString(), new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                progressView.hideProgress();
                view.showUser(user);
            }

            @Override
            public void failure(RetrofitError error) {
                progressView.hideProgress();
                progressView.showError(error.getMessage());
            }
        });
    }


}
