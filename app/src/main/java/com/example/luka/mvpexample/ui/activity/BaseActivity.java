package com.example.luka.mvpexample.ui.activity;

/**
 * Created by luka on 6.8.2015..
 */

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.mvp.view.BaseView;

class BaseActivity extends AppCompatActivity implements BaseView {


    protected MaterialDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }


    protected void showProgressDialog() {

        if (dialog == null || !dialog.isShowing()) {
            dialog = new MaterialDialog.Builder(this)
                    .progress(true, 0)
                    .show();
        }

    }


    protected void showProgressDialog(String title, String text) {

        if (dialog == null || !dialog.isShowing()) {
            dialog = new MaterialDialog.Builder(this)
                    .title(title)
                    .content(text)
                    .progress(true, 0)
                    .show();
        }

    }

    protected void hideProgressDialog() {

        if (dialog != null && dialog.isShowing()) {
            if (!isFinishing()) {
                dialog.hide();
            }
        }
    }

    protected void showDialog(String message) {
        dialog = new MaterialDialog.Builder(this)
                .title("Error")
                .content(message)
                .positiveText("Ok")
                .show();
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }

    @Override
    public void showProgress(String title, String text) {
        showProgressDialog(title, text);
    }

    @Override
    public void hideProgress() {
        hideProgressDialog();
    }

    @Override
    public void showError(String message) {
        showDialog(message);
    }


}