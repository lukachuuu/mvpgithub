package com.example.luka.mvpexample.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.mvp.view.IProgressView;
import com.example.luka.mvpexample.ui.activity.MainActivity;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FacebookLoginFragment extends BaseFragment implements IProgressView {

    private CallbackManager callbackManager;
    private ProfileTracker mProfileTracker;

    protected MaterialDialog dialog;

    @Bind(R.id.login_button)
    protected LoginButton facebook_login;


    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("Success FB login", "onSuccess");

            Profile profile = Profile.getCurrentProfile();
            Log.d("FB Profile", "onSuccess, profile = " + profile);


            String img = profile.getProfilePictureUri(100, 100).toString();


            PrefUtil.saveUserEmail("email", getActivity());
            Util.startActivityWithClearingActivityStack(getActivity(), MainActivity.class);
        }

        @Override
        public void onCancel() {
            showError("Login Cancel");
        }

        @Override
        public void onError(FacebookException e) {
            showError("onError, exception = " + e.toString());
        }
    };


    public FacebookLoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, mFacebookCallback);

        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

                if (newProfile != null) {
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    Log.d("Response FB", "onCompleted, response = " + response);
                                }
                            });

                    request.executeAsync();
                }
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_facebook_login, container, false);

        ButterKnife.bind(this, view);

        facebook_login.setCompoundDrawables(null, null, null, null);
        facebook_login.setBackgroundResource(android.R.color.transparent);
        facebook_login.setFragment(FacebookLoginFragment.this);
        facebook_login.setReadPermissions("public_profile", "email");
        facebook_login.registerCallback(callbackManager, mFacebookCallback);


        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void showProgress() {
        dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .show();

    }

    @Override
    public void showProgress(String title, String text) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(text)
                .progress(true, 0)
                .show();
    }

    @Override
    public void hideProgress() {
        this.dialog.hide();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("Ok")
                .show();
    }
}
