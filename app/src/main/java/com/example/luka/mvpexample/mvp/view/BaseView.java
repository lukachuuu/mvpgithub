package com.example.luka.mvpexample.mvp.view;

public interface BaseView {

    void showProgress();

    void showProgress(String title, String text);

    void hideProgress();

    void showError(String message);

}
