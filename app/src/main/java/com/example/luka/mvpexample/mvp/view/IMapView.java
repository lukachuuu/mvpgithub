package com.example.luka.mvpexample.mvp.view;

import android.location.Location;

/**
 * Created by luka on 26.8.2015..
 */
public interface IMapView {

    void showMeOnMap(Location mlocation);
}
