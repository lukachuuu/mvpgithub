package com.example.luka.mvpexample.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.database.AppUser;
import com.example.luka.mvpexample.ui.fragment.GitUsersListFragment;
import com.example.luka.mvpexample.ui.fragment.MapFragment;
import com.example.luka.mvpexample.util.CircleTransform;
import com.example.luka.mvpexample.util.PrefUtil;
import com.example.luka.mvpexample.util.Util;
import com.facebook.login.LoginManager;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Created by luka on 11.8.2015..
 */
public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Toolbar toolbar;


    @Bind(R.id.iv_avatar_drawer)
    ImageView avatarDraver;
    @Bind(R.id.tv_name_surname)
    TextView tvNameSurname;
    @Bind(R.id.tv_email_header)
    TextView tvEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.Member_list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        createDrawer(toolbar);

        ButterKnife.bind(this);

        tvNameSurname.setText("Luka Basek");
        tvEmail.setText("luka.basek@gmail.com");

        if (PrefUtil.getUserImg(getApplicationContext()).isEmpty())
            avatarDraver.setImageResource(R.drawable.ic_account_box_white_48dp);
        else {
            Picasso.with(getApplicationContext()).load(PrefUtil.getUserImg(getApplicationContext())).transform(new CircleTransform()).into(avatarDraver);
        }

        Realm realm = Realm.getInstance(this);
        RealmResults<AppUser> query = realm.where(AppUser.class)
                .findAll();

        for (AppUser p : query) {
            Log.d("User", p.getName() + " " + p.getEmail());
        }


        Fragment fragment = GitUsersListFragment.newInstance();
        addFragment(fragment);
    }

    private void addFragment(Fragment fragment) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.fragment_placeholder, fragment);
        transaction.commit();
    }

    private void createDrawer(Toolbar toolbar) {
        new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHeader(R.layout.header_drawer)
                .withHeaderClickable(true)
                .withHeaderDivider(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new SecondaryDrawerItem().withName("FIND A SPACE").withIcon(R.drawable.ic_account_box_white_48dp),
                        new SecondaryDrawerItem().withName("BROWSE SPACES").withIcon(R.drawable.com_facebook_button_like_icon_selected),
                        new SecondaryDrawerItem().withName("REQUESTS").withIcon(R.drawable.ic_accessibility_white_48dp),
                        new SecondaryDrawerItem().withName("BOOKINGS").withIcon(R.drawable.ic_android_white_48dp),
                        new SecondaryDrawerItem().withName("SETTINGS").withIcon(R.drawable.ic_list_white_48dp),
                        new SecondaryDrawerItem().withName("SWITCH ACCOUNT").withIcon(R.drawable.ic_power_settings_new_white_48dp))
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView, View view, int position, long l, IDrawerItem iDrawerItem) {

                        if (position == -1) {
                            Toast.makeText(getApplicationContext(), "HEADER", Toast.LENGTH_LONG).show();
                        } else if (position == 0) {
                            GitUsersListFragment fragment = new GitUsersListFragment();
                            FragmentManager fm = getSupportFragmentManager();
                            FragmentTransaction transaction = fm.beginTransaction();
                            transaction.replace(R.id.fragment_placeholder, fragment);
                            transaction.commit();
                        } else if (position == 1) {
                            MapFragment mapFragment = new MapFragment();
                            FragmentManager fm = getSupportFragmentManager();
                            FragmentTransaction transaction = fm.beginTransaction();
                            transaction.replace(R.id.fragment_placeholder, mapFragment);
                            transaction.commit();
                        } else if (position == 5) {
                            LoginManager.getInstance().logOut();
                            PrefUtil.deleteUserEmail(getApplicationContext());
                            PrefUtil.deleteUserImg(getApplicationContext());
                            Util.startActivityWithClearingActivityStack(MainActivity.this, TourActivity.class);
                        }

                        return false;
                    }
                })
                .build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
